#!/bin/sh 
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/ mvn \
      	"-Dexec.args=-classpath %classpath dom.jfischer.kaffeekasse.frontend.cmdline.Main $@" \
       	-Dexec.executable=/usr/lib/jvm/java-8-openjdk-amd64/bin/java \
	exec:exec
